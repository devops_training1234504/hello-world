
async function fetchGreetingsFromServer() {
    const response = await fetch('http://localhost:9000/', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            query: 'query {localTimeGreeting}'
        })
    })
    const body = await response.json()
    return body
}

fetchGreetingsFromServer().then((res) => {
        setTimeout(() => {
            document.getElementById('graphQLDataId').innerHTML = 'Loading .'
        }, 500)
        setTimeout(() => {
            document.getElementById('graphQLDataId').innerHTML = 'Loading . .'
        }, 1000)
        setTimeout(() => {
            document.getElementById('graphQLDataId').innerHTML = 'Loading . . .'
        }, 1500)
        setTimeout(() => {
            document.getElementById('graphQLDataId').innerHTML = res?.data?.localTimeGreeting
        }, 2000)
})


